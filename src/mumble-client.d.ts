declare module "mumble-client" {
  export default MumbleClient;
}

declare module "mumble-client-tcp" {
  export default async function connect(
    url: string,
    port: number,
    options: Object
  ): Promise<MumbleClient>;
}
