export class FakeResponse {
  statusCode: number;
  sent: boolean;
  jsonBody: any;

  status = (code: number) => {
    this.statusCode = code;
    return this;
  };

  json = (json: any) => {
    this.jsonBody = json;
    return this;
  };

  send = () => {
    this.sent = true;
    return this;
  };
}
