import MumbleClient from "mumble-client";
import { HegbotHandler } from "./hegbotHandlers";
import { info } from "../log";

export const echo: HegbotHandler = (client: MumbleClient) => {
  return {
    commands: ["echo"],
    handle: (args: String | undefined) => {
      if (args) {
        info(`echoing "${args}"`);
        if (args) {
          client.self.channel.sendMessage(args);
        }
      }
    },
  };
};
