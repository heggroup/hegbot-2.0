import { echo } from "./echo";

class FakeMumbleClient {
  messages: string[] = [];

  get self() {
    return {
      channel: {
        sendMessage: (msg: string) => this.messages.push(msg),
      },
    };
  }
}

describe("Echo Handler", () => {
  test("undefined message is a noop", () => {
    const client = new FakeMumbleClient();
    const handler = echo(client);
    handler.handle(undefined);
    expect(client.messages.length).toBe(0);
  });
  test("sends message", () => {
    const client = new FakeMumbleClient();
    const handler = echo(client);
    handler.handle("potatoes");
    expect(client.messages[0]).toBe("potatoes");
  });
});
