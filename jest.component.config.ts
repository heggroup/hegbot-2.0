import base from './jest.config';

export default {
  ...base,
  testMatch: [
    // "**/__tests__/**/*.[jt]s?(x)",
    "**/tests/**/?(*.)+(spec|test).[tj]s?(x)"
  ],
  globalSetup: "<rootDir>/tests/component-tests/startTestServer.js",
  globalTeardown: "<rootDir>/tests/component-tests/stopTestServer.js",
}