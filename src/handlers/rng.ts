import MumbleClient from "mumble-client";
import { HegbotHandler } from "./hegbotHandlers";
import { info } from "../log";

export const rng: HegbotHandler = (client: MumbleClient) => {
  return {
    commands: ["roll"],
    handle: (args: String | undefined) => {
      if (args) {
        const doubleSplitArgs = args.split(/[^0-9]/).filter((x) => x);
        var [faceCount, diceCount] = doubleSplitArgs
          .reverse()
          .map((s) => Number.parseInt(s));
        if (!diceCount || diceCount < 1) {
          diceCount = 1;
        }
        if (faceCount) {
          info(`rolling ${diceCount} D${faceCount}`);
          for (let i = 0; i < diceCount; i++) {
            if (args) {
              client.self.channel.sendMessage(
                Math.floor(Math.random() * faceCount) + 1
              );
            }
          }
        }
      }
    },
  };
};
