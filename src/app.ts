import bodyParser from "body-parser";
import cors from "cors";
import { Express } from "express";

export const configureApp = (app: Express) => {
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
};
