import { rng } from "./rng";

class FakeMumbleClient {
  messages: string[] = [];

  get self() {
    return {
      channel: {
        sendMessage: (msg: string) => this.messages.push(msg),
      },
    };
  }

  clearMessages() {
    this.messages = [];
  }
}

describe("Echo Handler", () => {
  test("undefined message is a noop", () => {
    const client = new FakeMumbleClient();
    const handler = rng(client);
    handler.handle(undefined);
    expect(client.messages.length).toBe(0);
  });

  test("can roll a single dice", () => {
    const client = new FakeMumbleClient();
    const handler = rng(client);
    handler.handle("5");
    expect(client.messages.length).toBe(1);
  });

  test("can roll multiple dice", () => {
    const client = new FakeMumbleClient();
    const handler = rng(client);
    for (let i = 1; i <= 5; i++) {
      handler.handle(`${i} 6`);
      expect(client.messages.length).toBe(i);
      client.clearMessages();
    }
  });

  test("accepts D before facecount", () => {
    const client = new FakeMumbleClient();
    const handler = rng(client);
    handler.handle("D10");
    expect(client.messages.length).toBe(1);
  });

  test("accepts roll xDy format", () => {
    const client = new FakeMumbleClient();
    const handler = rng(client);
    handler.handle("5D10");
    expect(client.messages.length).toBe(5);
  });

  test("parses side count correctly", () => {
    const client = new FakeMumbleClient();
    const handler = rng(client);
    handler.handle("1000D6");
    client.messages.forEach((num) => {
      const value = Number.parseInt(num);
      expect(value).toBeLessThan(7);
      expect(value).toBeGreaterThan(0);
    });
  });
});
