import { config } from "dotenv";
import { spawn } from "child_process";
import connect from "mumble-client-tcp";
import fs from "fs";

const startMumbleServer = async () => {
  if (process.env.CI) {
    console.log("starting alpine mumble server");
    const process = spawn("rc-service murmur start");
  } else {
    console.log("starting local mumble server");
    const process = spawn(
      "./tests/mumble-server/mumble_server-1.4.287.x64.macos", ["-ini", "tests/mumble-server/test-config.ini", "-fg", "-v"],
      // (error, stdout, stderr) => {
      //   if (error) {
      //     console.error(`exec error: ${error}`);
      //     return;
      //   }
      //   console.log(`stdout: ${stdout}`);
      //   console.error(`stderr: ${stderr}`);
      // },
      // {
      //   detached: true,
      // }
    )
    fs.writeFileSync("tests/mumble-server/temp/murmur.pid", process.pid.toString())
  }

  return new Promise((resolve, reject) => {
    let timeout;
    let task;

    timeout = setTimeout(() => {
      clearInterval(task);
      reject("Failed to connect to mumble server");
    }, 5000);

    task = setInterval(() => {
      connect("localhost", 64738, {
        username: "healthcheck",
        password: "",
        tls: {
          rejectUnauthorized: false,
        },
      })
        .then(resolve)
        .catch((e) => console.log(`Error connecting to mumble ${e}`));
    }, 500);
  });
};

export default async function startTestServer() {
  config({ path: "local.env", debug: true });

  const { startServer } = require("../../dist/startServer.js"); // needs to be imported after dotenv

  await startMumbleServer()
    .then(startServer)
    .then(
      (server) =>
        new Promise((resolve, reject) => {
          let lastStatusCode = "None";

          let timeout;
          let task;

          timeout = setTimeout(() => {
            clearInterval(task);
            reject(`No 200 response. last status: ${lastStatusCode}`);
          }, 3000);

          task = setInterval(() => {
            fetch(`http://localhost:3000/health`)
              .then((res) => {
                lastStatusCode = res.status.toString();
                if (res.status === 200) {
                  clearTimeout(timeout);
                  clearInterval(task);
                  resolve(server);
                }
              })
              .catch((e) => console.log(`Error in server healthcheck ${e}`));
          }, 500);
        })
    );
}
