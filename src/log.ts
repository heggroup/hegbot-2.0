export const info = (s: string) => {
  console.log(`${new Date().toLocaleString()} - ${s}`);
};
