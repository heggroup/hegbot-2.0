import { registerHegbotHandlers } from "./hegbotHandlers";
import { Express } from "express";
import MumbleClient from "mumble-client";

class FakeExpress {
  handlers: Function[];

  constructor() {
    this.handlers = [];
  }

  post(_cmd: string, f: Function) {
    this.handlers.push(f);
  }
}

describe("HegBot Handlers", () => {
  test("httpify works", () => {
    const app = new FakeExpress();
    registerHegbotHandlers(
      app as unknown as Express,
      undefined as unknown as typeof MumbleClient,
      [
        () => {
          return {
            commands: ["test"],
            handle: () => {
              text: "test";
            },
          };
        },
      ]
    );
    expect(app.handlers.length).toBe(1);
  });

  test("httpify registers multiple commands", () => {
    const app = new FakeExpress();
    registerHegbotHandlers(
      app as unknown as Express,
      undefined as unknown as typeof MumbleClient,
      [
        () => {
          return {
            commands: ["test", "test2"],
            handle: () => {
              text: "test";
            },
          };
        },
      ]
    );
    expect(app.handlers.length).toBe(2);
  });
});
