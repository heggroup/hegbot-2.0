import { Express } from "express";
import { healthcheck, version, shutdown } from "./base";

export const addHandlers = (
  app: Express,
  masterUser: { username: string; password: string }
) => {
  app.get(`/health`, healthcheck);
  app.get(`/version`, version);
  app.post(`/shutdown`, shutdown(masterUser));
};
