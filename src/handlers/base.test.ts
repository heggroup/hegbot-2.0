import { healthcheck, version } from "./base";
import { Request, Response } from "express";
import { FakeResponse } from "../../tests/utils/mocks";

describe("Base Handlers", () => {
  test("Healthcheck returns 200", () => {
    const fakeResponse = new FakeResponse();
    healthcheck(
      undefined as unknown as Request,
      fakeResponse as unknown as Response
    );
    expect(fakeResponse.statusCode).toBe(200);
    expect(fakeResponse.sent).toBeTruthy();
  });

  test("Version returns 200 with version", () => {
    const fakeResponse = new FakeResponse();
    version(
      undefined as unknown as Request,
      fakeResponse as unknown as Response
    );
    expect(fakeResponse.statusCode).toBe(200);
    expect(Object.keys(fakeResponse.jsonBody)).toContain("version");
    expect(fakeResponse.sent).toBeTruthy();
  });
});
