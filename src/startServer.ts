import express from "express";
import { addHandlers } from "./handlers/addHandlers";
import { configureApp } from "./app";
import { Server } from "http";
import { info } from "./log";
import connect from "mumble-client-tcp";
import MumbleClient from "mumble-client";
import { registerHegbotHandlers } from "./handlers/hegbotHandlers";

const connectToMumble = async (
  url: string,
  port: number,
  username: string,
  password?: string | undefined
) => {
  return connect(url, port, {
    username: username,
    password: password,
    tls: {
      rejectUnauthorized: false,
    },
  }).catch((err) =>
    console.error(`Failed to connect to mumble server: ${err}`)
  );
};

export const startServer = async (): Promise<Server> => {
  const hegbotPort = process.env.PORT!!;
  const mumbleUrl = process.env.MUMBLE_SERVER_URL!!;
  const mumblePort = Number.parseInt(process.env.MUMBLE_SERVER_PORT ?? "64738");
  const mumblePassword = process.env.PASSWORD;
  const username = process.env.USERNAME ?? "HegBot";
  const masterUser = {
    username: process.env.MASTER_USERNAME!!,
    password: process.env.MASTER_PASSWORD!!,
  };

  if (!hegbotPort || !masterUser.username || !masterUser.password) {
    info("Missing key config");
    process.exit(-1);
  }

  const client: MumbleClient = await connectToMumble(
    mumbleUrl,
    mumblePort,
    username,
    mumblePassword
  );

  console.log(client.root.sendMessage("HegBot Online."));

  const app = express();
  configureApp(app);

  addHandlers(app, masterUser);
  registerHegbotHandlers(app, client);

  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      reject(`Failed to start server`);
    }, 10000);

    var server: Server | undefined;
    try {
      server = app.listen(hegbotPort, () => {
        info(`HegBot listening on http://localhost:${hegbotPort}`);
      });
      clearTimeout(timeout);
      resolve(server);
    } catch (e) {
      server?.close();
      info(`Failed to start server - ${e}`);
      info(` Retrying in 1 second`);
      setTimeout(startServer, 1000);
    }
  });
};
