import { Express, Request, Response } from "express";
import MumbleClient from "mumble-client";
import { echo } from "./echo";
import { info } from "../log";
import { rng } from "./rng";

type InitialisedHandler = {
  commands: string[];
  handle: (args: string | undefined) => any;
};

export type HegbotHandler = (client: MumbleClient) => InitialisedHandler;

const productionHandlers: HegbotHandler[] = [echo, rng];

const httpify = (h: InitialisedHandler) => (req: Request, res: Response) => {
  res.json(h.handle(req.query.args?.toString()));
};

export const registerHegbotHandlers = (
  app: Express,
  client: MumbleClient,
  handlers: HegbotHandler[] = productionHandlers
) => {
  handlers.forEach((h) => {
    const handler = h(client);
    info(`Registering ${handler.commands[0]} handler`);
    handler.commands.forEach((command) => {
      app.post(`/${command}`, httpify(handler));
    });
  });
};
