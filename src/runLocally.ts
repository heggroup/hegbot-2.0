import { config } from "dotenv";

config({ path: "local.env" });

import { startServer } from "./startServer";

startServer();
