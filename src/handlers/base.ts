import { Request, Response } from "express";
import { info } from "../log";

export const healthcheck = (_req: Request, res: Response) => {
  res.status(200).send();
};

export const version = (_req: Request, res: Response) => {
  res.status(200).json({ version: process.env.npm_package_version }).send();
};

export const shutdown =
  (adminLogin: { username: string; password: string }) =>
  (req: Request, res: Response) => {
    const { username, password } = req.body;
    if (username === adminLogin.username && password === adminLogin.password) {
      info("Shutting down...");
      res.status(200).send();
      process.exit(0);
    } else {
      info("Unauthorised shutdown request");
      res.status(401).send();
    }
  };
