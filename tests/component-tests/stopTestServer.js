const stopMumbleServer = () => {
  console.log("STOPPING MUMBLE SERVER")
  if (process.env.CI) {
    return exec("rc-service murmur stop");
  } else {
    const pid = Number.parseInt(
      fs.readFileSync("./tests/mumble-server/temp/murmur.pid").toString()
    );
    console.log(`PID: ${pid}`)
    return process.kill(pid);
  }
};

export default async function stopTestServer() {
  console.log("fuck")
  return new Promise((resolve, reject) => {
    let lastStatusCode = "None";

    const masterUser = {
      username: process.env.MASTER_USERNAME,
      password: process.env.MASTER_PASSWORD,
    };

    let timeout;
    let task;

    timeout = setTimeout(() => {
      clearInterval(task);
      reject(`No 200 response. last status: ${lastStatusCode}`);
    }, 3000);

    task = setInterval(() => {
      fetch(`http://localhost:3000/shutdown`, {
        method: "POST",
        body: JSON.stringify(masterUser),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((res) => {
          lastStatusCode = res.status.toString();
          console.log("shit")
          if (res.status === 200) {
            clearTimeout(timeout);
            clearInterval(task);
            resolve(server);
          }
        })
        .catch((e) => console.log(`Error in shutdown request ${e}`))
        .finally(stopMumbleServer);
    }, 500);
  })
}
